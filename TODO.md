### Changes to implement in future versions of Pegacorn Project

##### Improve my testing skills 
- [ ] Test non-private methods for all files
- [ ] Research how to write integration tests in order to test private methods

##### Refresh on my ability to write a new API client, improve consistency among files 
- [ ] Rewrite watch_chat_waits to use REST API instead of Streaming API 

##### Improve my networking skills
- [ ] Get RSG internet working on RaspPi! 
- [ ] Connect RaspPi to Pegacorn 

