require 'count_good_ratings'
require 'rspec'

describe GoodRatingsCount do
  context '(smoke test) spec can find class' do
    let(:new_rating) { GoodRatingsCount.new }
    it 'GoodRatingsCount' do
      @http_response = new_rating.fetch_ratings_from_zendesk
      expect(GoodRatingsCount.new).not_to be_nil
    end
  end

  context 'enough good ratings triggers pegacorn lighter' do
    let(:new_rating) { GoodRatingsCount.new }
    it 'NEEDS WORK -- 301 satisfaction ratings satisfies #satisfaction_goal_reached?' do
      @http_response = new_rating.fetch_ratings_from_zendesk
      # @http_response = JSON.parse(@http_response.body)
      @http_response['satisfaction_ratings'] = 1301
      puts 'HTTP RESPONSE: ' + @http_response.to_s
      expect(new_rating.send(:satisfaction_goal_reached?, @http_response)).to be_truthy
    end
  end

  context '#write_to_log_file:' do
    let(:new_rating) { GoodRatingsCount.new }
    it 'affects stdout/log file' do
      @http_response = new_rating.fetch_ratings_from_zendesk
      expect { new_rating.write_to_log_file(@http_response) }.to \
        output(/.*/).to_stdout
    end
  end
end
