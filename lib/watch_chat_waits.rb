require 'faye/websocket'
require 'eventmachine'
require '/Users/tyoung/workspace/Pegacorn_Project/.gitignore/pegacorn_secrets'
require 'json'
require 'pi_piper'
require 'date'

class WatchChatWaits
  NUMBER_OF_TRIES = 3

  def create_new_request
    websocket_client =
      Faye::WebSocket::Client.new('wss://rtm.zopim.com/stream',
                                  nil,
                                  headers: authorization_credentials)
    websocket_client
  end

  def subscribe_to_chat_waits(websocket_client)
    websocket_client.on :open do
      puts 'Starting...'
      msg = {}
      msg[:topic] = 'chats.waiting_time_avg'
      msg[:action] = 'subscribe'
      websocket_client.send msg.to_json
    end
  end

  def check_for_wait_time_on_message(websocket_client)
    websocket_client.on :message do |event|
      hashed_json_message = JSON.parse(event.data).to_hash
      wait_time_avg = dig_for_wait_time_avg(hashed_json_message)
      try_then_log_result(wait_time_avg, NUMBER_OF_TRIES)
    end
  end

  def log_status_on_close(websocket_client)
    websocket_client.on :close do |event|
      if event.code != 1006
        puts "Something went wrong. Event code: #{event.code}."
      end
      puts '...Closing.'
      EM.stop
    end
  end

  private

  def try_then_log_result(wait_time_avg, number_tries)
    if !wait_time_avg.nil?
      manage_rasp_pi(wait_time_avg)
    elsif wait_time_avg.nil? && number_tries.positive?
      number_tries -= 1
      sleep 5
      try_then_log_result(websocket_client, number_tries)
    else log_attempt_failure
    end
    websocket_client = true
    EM.stop
  end

  def manage_rasp_pi(wait_time_avg)
    if wait_time_avg_goal_reached?(wait_time_avg)
      log_success_light_rasp_pi(wait_time_avg)
    else
      log_goal_not_reached(wait_time_avg)
    end
  end

  def log_success_light_rasp_pi(wait_time_avg)
    puts "It's currently #{DateTime.now}."
    puts "Chat wait time average is #{wait_time_avg} seconds"
    puts "\nLight the pegacorn!\n"
    # light_it_up
  end

  def log_goal_not_reached(wait_time_avg)
    puts "Chat wait time average is #{wait_time_avg} seconds"
    puts 'Wait time goal not reached - maybe next time!'
  end

  def dig_for_wait_time_avg(hashed_json_message)
    [hashed_json_message.dig('content', 'data', 'waiting_time_avg')][0].to_i
  end

  def log_attempt_failure
    puts "It's currently #{DateTime.now}."
    puts 'Chat wait time average is not available.'
  end

  def wait_time_avg_goal_reached?(wait_time_avg)
    return true if wait_time_avg > 1 && wait_time_avg < 45
  end

  def authorization_credentials
    { 'Authorization' => "Bearer #{ZendeskSecrets::ZENDESK_OAUTH_ACCESS_TOKEN}" }
  end

  def light_it_up
    pin.off
    pin.on
    sleep 15 # seconds
    pin.off
  end

  def pin
    PiPiper::Pin.new(pin: 17, direction: :out)
  end
end

wait_watcher = WatchChatWaits.new
EM.run do
  new_client = wait_watcher.create_new_request
  wait_watcher.subscribe_to_chat_waits(new_client)
  wait_watcher.check_for_wait_time_on_message(new_client)
  wait_watcher.log_status_on_close(new_client)
end
